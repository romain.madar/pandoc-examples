## Convert jupyter notebook into markdown format
jupyter nbconvert --to markdown src/BtagginStudies.ipynb
jupyter nbconvert --to markdown src/DarkMatterModelExploration.ipynb
jupyter nbconvert --to markdown src/GaussianProcesses.ipynb
jupyter nbconvert --to markdown src/4top_ttV_classifier.ipynb

## Concatenate and convert all notebook into a single pdf using latex
## using different type of option. The file  contains
## usefil metadata (title, subtitle, author, abstract, etc ...)

# 1. LaTeX book with personal code highlighting
cd src/.
pandoc -N --template="../template/general_text_template.tex"  DarkMatterModelExploration.md BtagginStudies.md GaussianProcesses.md 4top_ttV_classifier.md --toc -o ../results/example-book-customhighlight.pdf --variable geometry="a4paper, total={6in, 8in}" -V linestretch="1.2" --listings --variable documentclass="book"

# 2. LaTeX article with predifined code highlighting (named 'kate')
pandoc -N --template="../template/general_text_template.tex"  DarkMatterModelExploration.md BtagginStudies.md GaussianProcesses.md 4top_ttV_classifier.md --toc -o ../results/example-article.pdf --variable geometry="a4paper, total={6in, 8in}" -V linestretch="1.2" --highlight-style kate

# 3. Latex article without table of contents and very large margins
pandoc -N --template="../template/general_text_template.tex"  DarkMatterModelExploration.md BtagginStudies.md GaussianProcesses.md 4top_ttV_classifier.md -o ../results/example-article-notoc.pdf --variable geometry="a4paper, total={5in, 7in}" -V linestretch="0.9" --highlight-style kate

# 4. Beamer slides in one command with playing with various options
jupyter nbconvert --to markdown CUseminaires.ipynb  --stdout | pandoc -N --to beamer --template="../template/general_beamer_template.tex" -o ../results/beamer-slides.pdf --slide-level=2 -V theme:"CambridgeUS" -V colortheme="seagull"  -V fonttheme="structurebold" -V colorlinks -V toc -V fontfamily="libertine"

cd -

