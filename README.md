# Pandoc examples

[Pandoc](https://pandoc.org/) is a library which converts text format into other formats, like markdown to latex, odt or html. 
This repository implement a couple of examples based on jupyter notebook, since they can be exported into markdown format. 
Books, articles and beamer slides are given as examples based on the corresponding notebook.

## Structure of the repository

The folder `src` contains all the notebook and a markdown file for the meta-data of the latex pdf. 
The folder `template` contains files that are needed to define the various latex option (base ot build the `.tex` file). 
The folder `result` contains the pdf files.

## Building the pdf examples

You first need to clone this repository and go in it
```
git clone git clone https://github.com/rmadar/pandoc-examples
cd pandoc-examples
```

Then you can run all the example at once by doing
```
source build.sh
```
## Playing with the options

By looking at the command lines in `build.sh` and the [pandoc help manual](https://pandoc.org/MANUAL.html), it's easy to personalize the pdf you get out of the latex conversion.
