---
title: "Example of two notebooks converted into a latex document"
author: [Romain Madar]
date: 2018-02-14
subject: "Markdown"
tags: [Markdown, Example]
subtitle: "A hand-on tutorial on notebook conversion"
abstract: "Lorem ipsum dolor sit amet, cibo doming numquam mei et, sea menandri oportere id, an dictas feugait partiendo est. Ius in hinc epicurei, vim facer prompta neglegentur ad. Idque tritani sententiae te eum, sea quodsi phaedrum cu. Te vim regione salutandi erroribus."
institute: "Laboratoire de Physique de Clermont"
#titlepage: true
...

